The application is build with Spring Boot, Maven, H2 database and some prefered libraries which non-technical readers shouldn't be concerned about (for technical readers see the Technical Info part bellow).


Technical Information
- Lombok library is heavily used and to not have problems with intellij please following link. 
https://stackoverflow.com/a/41161107/6313907
- JPA backed by Hibernate is used since read is simple and we don't have complex queries in place for demo.
- To start the app mvn spring-boot:run
- To access in memory H2 database use http://localhost:8081/h2-console with username=sa and not putting any character for password. See H2_console.png

    TODO LIST
    -Implement caching
    -Write a service for finding average, min, max
    -Write Unit tests 
    -Write Integration tests
    -Write request validation tests
