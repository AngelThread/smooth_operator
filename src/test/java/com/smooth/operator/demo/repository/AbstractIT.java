package com.smooth.operator.demo.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@Profile("test")
public class AbstractIT {

    @Autowired
    MachineRepository machineRepository;

    @Autowired
    ParameterRepository parameterRepository;

    @BeforeEach
    public void prepare() {
        cleanDB();
    }

    private void cleanDB() {
        parameterRepository.deleteAll();
        machineRepository.deleteAll();
    }

    @AfterEach
    public void cleanUp() {
        cleanDB();
    }
}
