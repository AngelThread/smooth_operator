package com.smooth.operator.demo.repository;

import com.smooth.operator.demo.entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


public class MachineRepositoryIT extends AbstractIT{

    @Autowired
    private MachineParameterValueRepository machineParameterValueRepository;

    @Test
    public void addingNewMachine() {
        String key = "machine-1";
        MachineEntity machineOne = MachineEntity.builder()
                .key(key)
                .name("Toy-1")
                .build();
        MachineEntity savedMachine = machineRepository.save(machineOne);

        assertThat(machineRepository.findById(savedMachine.getId()))
                .isPresent();
        assertThat(machineRepository.findById(savedMachine.getId()).get().getKey())
                .isEqualTo(key);
    }


    @Test
    public void addingNewMachine_addingNewParameter_checkingParameterPersisted() {
        String machineKey = "machine-1";
        String parameterKey = "heat";

        MachineEntity machineOne = MachineEntity.builder()
                .key(machineKey)
                .name("Toy-1")
                .build();
        MachineEntity savedMachine = machineRepository.save(machineOne);

        assertThat(machineRepository.findById(savedMachine.getId()))
                .isPresent();
        assertThat(machineRepository.findById(savedMachine.getId()).get().getKey())
                .isEqualTo(machineKey);
        //add new parameter
        ParameterEntity parameterEntity = ParameterEntity.builder()
                .key(parameterKey)
                .name("Heat")
                .type("real")
                .unit("joule")
                .build();
        parameterRepository.save(parameterEntity);
        //link machine and the new parameter
        machineOne.addParameter(parameterEntity);
        machineRepository.save(machineOne);
        //find all parameters of the machine
        Optional<MachineEntity> byMachineKeyWithParameters = machineRepository
                .findByMachineAndParameterKeyWithParameters(savedMachine.getKey());
        assertThat(byMachineKeyWithParameters).isPresent();
        assertThat(byMachineKeyWithParameters.get().getParameters()).hasSize(1);
        MachineEntity existingMachine = byMachineKeyWithParameters.get();
        //machine parameters
        List<MachineParameterEntity> machineParameterEntities = new ArrayList<>(byMachineKeyWithParameters.get()
                .getParameters());
        String savedParameterKey = machineParameterEntities.get(0).getParameter().getKey();
        assertThat(savedParameterKey).isEqualTo(parameterKey);
        //create a new value for the machine parameter
        MachineParameterValueEntity machineParameterValue = MachineParameterValueEntity.builder()
                .machineParameter(machineParameterEntities.get(0))
                .value("10")
                .createdAt(LocalDateTime.now().minusDays(1))
                .build();

        Optional<MachineParameterEntity> byMachineKeyWithParameter = machineRepository
                .findByMachineAndParameterKeyWithParameters(machineKey, parameterKey);
        assertThat(byMachineKeyWithParameter).isPresent();
        MachineParameterEntity machineParameterEntity = byMachineKeyWithParameter.get();

        machineParameterValue.setMachineParameter(machineParameterEntity);
        MachineParameterValueEntity savedParameterValue = machineParameterValueRepository.save(machineParameterValue);

        MachineParameterValueEntity machineParameterValueSecond = MachineParameterValueEntity.builder()
                .machineParameter(machineParameterEntities.get(0))
                .value("12")
                .build();

        List<MachineParameterValueEntity> latestValuesOfParameter =
                machineParameterValueRepository.findLatestValuesOfParameter();

        assertThat(latestValuesOfParameter).hasSize(1);
    }
}
