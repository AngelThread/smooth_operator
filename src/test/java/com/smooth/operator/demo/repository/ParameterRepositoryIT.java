package com.smooth.operator.demo.repository;

import com.smooth.operator.demo.entity.ParameterEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@Profile("test")
public class ParameterRepositoryIT {
    @Autowired
    private ParameterRepository parameterRepository;

    @Test
    public void addingNewMachine() {
        String parameterKey = "heat";
        ParameterEntity parameterEntity = ParameterEntity.builder()
                .key(parameterKey)
                .name("Heat")
                .type("real")
                .unit("joule")
                .build();
        ParameterEntity savedParameter = parameterRepository.save(parameterEntity);
        Optional<ParameterEntity> foundParameter = parameterRepository.findById(savedParameter.getId());
        assertThat(foundParameter).isPresent();
        assertThat(foundParameter.get().getKey()).isEqualTo(parameterKey);
    }
}
