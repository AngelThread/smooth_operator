package com.smooth.operator.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class MachineMetaDataDTO {
    private String key;
    private String name;
    private Set<ParameterDTO> parameters = new HashSet<>();
}
