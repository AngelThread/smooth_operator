package com.smooth.operator.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ParameterDTO {
    private String key;
    private String machineKey;
    private String name;
    private String type;
    private String unit;
}
