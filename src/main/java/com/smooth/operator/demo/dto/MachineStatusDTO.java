package com.smooth.operator.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Map;

@Data
@NoArgsConstructor
public class MachineStatusDTO {
    //Represents the unique identifier of the machine
    @NonNull
    @JsonProperty("machineKey")
    private String machineKey;
    @NonNull
    private Map<String, String> parameters;
}
