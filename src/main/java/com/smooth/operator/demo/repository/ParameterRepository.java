package com.smooth.operator.demo.repository;

import com.smooth.operator.demo.entity.ParameterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ParameterRepository extends JpaRepository<ParameterEntity,Long> {
    Optional<ParameterEntity> findByKey(String key);

    @Query(value = "SELECT p FROM ParameterEntity p LEFT JOIN "
            + "FETCH p.machines where p.key = :key")
    Optional<ParameterEntity> findByKeyWithMachines(@Param("key") String key);
}
