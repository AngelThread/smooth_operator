package com.smooth.operator.demo.repository;

import com.smooth.operator.demo.entity.MachineEntity;
import com.smooth.operator.demo.entity.MachineParameterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MachineRepository extends JpaRepository<MachineEntity, Long> {
    @Query(value = "SELECT e FROM MachineEntity e LEFT JOIN "
            + "FETCH e.parameters ps LEFT JOIN FETCH ps.parameter where e.key = :key")
    Optional<MachineEntity> findByMachineAndParameterKeyWithParameters(@Param("key") String key);


    @Query(value = "SELECT ps FROM MachineEntity e LEFT JOIN "
            + " e.parameters ps LEFT JOIN ps.parameter psp where e.key = :machineKey " +
            "and psp.key= :parameterKey")
    Optional<MachineParameterEntity> findByMachineAndParameterKeyWithParameters(@Param("machineKey") String key,
                                                                                @Param("parameterKey") String parameterKey);

    @Query(value = "SELECT m FROM MachineEntity m LEFT JOIN "
            + " m.parameters ps LEFT JOIN ps.parameter psp")
    List<MachineEntity> findAllWithParameters();

}
