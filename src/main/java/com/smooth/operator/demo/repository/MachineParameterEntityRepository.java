package com.smooth.operator.demo.repository;

import com.smooth.operator.demo.entity.MachineParameterEntity;
import com.smooth.operator.demo.entity.MachineParameterId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MachineParameterEntityRepository extends JpaRepository<MachineParameterEntity, MachineParameterId> {
}
