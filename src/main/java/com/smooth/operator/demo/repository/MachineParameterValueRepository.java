package com.smooth.operator.demo.repository;

import com.smooth.operator.demo.entity.MachineParameterValueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MachineParameterValueRepository extends JpaRepository<MachineParameterValueEntity, Long> {

    @Query(value = "SELECT v FROM MachineParameterValueEntity v group by v.machineParameter order by max(v.createdAt)")
    List<MachineParameterValueEntity> findLatestValuesOfParameter();

}

