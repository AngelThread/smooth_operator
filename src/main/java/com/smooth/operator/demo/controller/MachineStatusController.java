package com.smooth.operator.demo.controller;

import com.smooth.operator.demo.dto.MachineStatusDTO;
import com.smooth.operator.demo.entity.MachineEntity;
import com.smooth.operator.demo.entity.MachineParameterEntity;
import com.smooth.operator.demo.exception.MachineParameterCombinationNotFound;
import com.smooth.operator.demo.mapper.MachineStatusMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
//TODO add swagger or other rest api doc implementation.

/**
 * This controller is responsible of machine status related operations and provides API for outer world.
 */
@RestController
@RequiredArgsConstructor
public class MachineStatusController {

    private final MachineStatusService machineStatusService;
    private final MachineStatusMapper machineStatusMapper;

    @PostMapping
    public ResponseEntity<MachineStatusDTO> saveMachineStatus(@RequestBody @NonNull
                                                              @Valid MachineStatusDTO machineStatusDTO)
            throws MachineParameterCombinationNotFound {
        machineStatusService.addStatus(machineStatusMapper.toMachineStatusData(machineStatusDTO));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<MachineEntity>> getMachineStatus() {
        List<MachineEntity> allMachineStatus = machineStatusService.getAllMachineStatus();
        //TODO convert entity to DTO and prevent cycling/recursive calls.
        //At the moment there is a recursive call.

        return new ResponseEntity<List<MachineEntity>>(allMachineStatus, HttpStatus.OK);
    }

    //TODO write another endpoint for average, median, min and max of each parameter
}
