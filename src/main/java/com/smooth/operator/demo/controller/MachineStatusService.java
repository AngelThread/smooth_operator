package com.smooth.operator.demo.controller;

import com.smooth.operator.demo.data.MachineStatusData;
import com.smooth.operator.demo.entity.*;
import com.smooth.operator.demo.exception.MachineParameterCombinationNotFound;
import com.smooth.operator.demo.exception.NotValidParameterTypeException;
import com.smooth.operator.demo.repository.MachineParameterEntityRepository;
import com.smooth.operator.demo.repository.MachineParameterValueRepository;
import com.smooth.operator.demo.repository.MachineRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Service responsible of managing machine status.
 */
@Service
@RequiredArgsConstructor
public class MachineStatusService {

    private final MachineRepository machineRepository;
    private final MachineParameterEntityRepository machineParameterEntityRepository;
    private final MachineParameterValueRepository machineParameterValueRepository;

    @Transactional
    public void addStatus(@NonNull MachineStatusData machineStatusData) throws MachineParameterCombinationNotFound {
        final String machineKey = machineStatusData.getMachineKey();

        for (Map.Entry<String, String> parameter : machineStatusData.getParameters().entrySet()) {
            //Even if one wrong parameter doesn't add any statuses!
            checkParameterValue(parameter);
            MachineParameterEntity machineParameterEntity = machineRepository.
                    findByMachineAndParameterKeyWithParameters(machineKey, parameter.getKey())
                    .orElseThrow(MachineParameterCombinationNotFound::new);
            //Add new value
            machineParameterEntity.addValue(MachineParameterValueEntity.builder().value(parameter.getValue()).build());
        }
    }

    private void checkParameterValue(Map.Entry<String, String> parameter) {
        ParameterType parameterType = ParameterType.valueOf(parameter.getKey().toUpperCase());
        boolean isValid = parameterType.checkParameterType(parameter.getValue());
        if (!isValid) {
            throw new NotValidParameterTypeException("Parameter is not valid!Key:" + parameter.getKey() + " Value:"
                    + parameter.getValue());
        }
    }

    public List<MachineEntity> getAllMachineStatus() {
        List<MachineEntity> allWithParameters = machineRepository.findAllWithParameters();
        return allWithParameters;

    }
}
