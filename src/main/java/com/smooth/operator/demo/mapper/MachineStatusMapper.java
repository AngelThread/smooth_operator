package com.smooth.operator.demo.mapper;

import com.smooth.operator.demo.data.MachineStatusData;
import com.smooth.operator.demo.dto.MachineStatusDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MachineStatusMapper {

    MachineStatusData toMachineStatusData(MachineStatusDTO machineStatusDTO);
}
