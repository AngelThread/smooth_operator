package com.smooth.operator.demo.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.smooth.operator.demo.data.MachineInputData;
import com.smooth.operator.demo.data.ParameterInputData;
import com.smooth.operator.demo.entity.MachineEntity;
import com.smooth.operator.demo.repository.MachineRepository;
import com.smooth.operator.demo.entity.ParameterEntity;
import com.smooth.operator.demo.repository.ParameterRepository;
import com.smooth.operator.demo.exception.DataLoadException;
import com.smooth.operator.demo.exception.MachineNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

/**
 * This service is responsible of uploading metadate of machines and their parameters to in memory db.
 */
@Service
@RequiredArgsConstructor
public class DataLoader {
    //TODO crease MachineService and use its methods instead of repository.
    private final MachineRepository machineRepository;
    //TODO crease ParameterService and use its methods instead of repository.
    private final ParameterRepository parameterRepository;

    @PostConstruct
    public void loadData() {
        loadMachines("/static/machines.csv");
        loadParameters("/static/parameters.csv");
    }


    private void loadMachines(String fileName) {
        try {
            MappingIterator<MachineInputData> machineDTOMappingIterator = getInputData(fileName, MachineInputData.class);
            int rowCount = 0;
            while (machineDTOMappingIterator.hasNextValue()) {
                if (rowCount == 0) {
                    //Ignoring header
                    machineDTOMappingIterator.nextValue();
                    rowCount++;
                    continue;
                }
                MachineInputData machineValue = machineDTOMappingIterator.nextValue();
                //TODO use mapstruct mapper to map file line to MachineEntity
                //TODO call MachineService instead of machineRepository
                machineRepository.save(MachineEntity.builder()
                        .key(machineValue.getKey())
                        .name(machineValue.getName())
                        .build());
            }
        } catch (IOException e) {
            throw new DataLoadException("Machine information couldn't be loaded!", e);
        }
    }

    private <T> MappingIterator<T> getInputData(String fileName, Class<T> clazz) throws IOException {
        BufferedReader machines = new BufferedReader(
                new InputStreamReader(getClass().getResourceAsStream(fileName)));
        return new CsvMapper()
                .readerWithTypedSchemaFor(clazz)
                .readValues(machines);
    }

    private void loadParameters(String fileName) {
        try {
            MappingIterator<ParameterInputData> parameterInputDataMappingIterator =
                    getInputData(fileName, ParameterInputData.class);
            int rowCount = 0;
            while (parameterInputDataMappingIterator.hasNextValue()) {
                if (rowCount == 0) {
                    //Ignoring header
                    parameterInputDataMappingIterator.nextValue();
                    rowCount++;
                    continue;
                }
                ParameterInputData parameterInputData = parameterInputDataMappingIterator.nextValue();
                String machineKey = parameterInputData.getMachineKey();

                MachineEntity machineEntity = machineRepository.
                        findByMachineAndParameterKeyWithParameters(machineKey).orElseThrow(() ->
                        new MachineNotFoundException("Machine not found with key:" + machineKey));
                ParameterEntity savedParameterEntity = null;
                Optional<ParameterEntity> savedParameter = parameterRepository
                        .findByKeyWithMachines(parameterInputData.getKey());

                if (!savedParameter.isPresent()) {
                    savedParameterEntity = parameterRepository.save(createNewParameterEntity(parameterInputData));
                } else {
                    savedParameterEntity = savedParameter.get();
                }

                machineEntity.addParameter(savedParameterEntity);
                machineRepository.save(machineEntity);
            }
        } catch (IOException e) {
            throw new DataLoadException("Parameter information couldn't be loaded!", e);
        }
    }

    private ParameterEntity createNewParameterEntity(ParameterInputData parameterInputData) {
        return ParameterEntity.builder()
                .key(parameterInputData.getKey())
                .name(parameterInputData.getName())
                .unit(parameterInputData.getUnit())
                .type(parameterInputData.getType())
                .build();
    }
}
