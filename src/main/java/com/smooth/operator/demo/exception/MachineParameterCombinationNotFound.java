package com.smooth.operator.demo.exception;

/**
 * This exception represents the case sent machine key and parameter key is not
 * in the system.
 */
public class MachineParameterCombinationNotFound extends Exception {
}
