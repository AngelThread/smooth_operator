package com.smooth.operator.demo.exception;

public class DataLoadException extends RuntimeException {
    public DataLoadException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
