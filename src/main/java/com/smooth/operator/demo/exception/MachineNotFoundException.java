package com.smooth.operator.demo.exception;

public class MachineNotFoundException extends RuntimeException {
    public MachineNotFoundException(String message) {
        super(message);
    }
    public MachineNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
