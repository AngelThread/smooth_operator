package com.smooth.operator.demo.exception;

public class NotValidParameterTypeException extends RuntimeException {
    public NotValidParameterTypeException(String message) {
        super(message);
    }
}
