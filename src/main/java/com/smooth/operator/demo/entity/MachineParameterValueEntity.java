package com.smooth.operator.demo.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "machine_parameter_values", indexes = {
        @Index(columnList = "created_at", name = "ind_machine_parameter_values_createdAt")})
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"machineParameter"})
@ToString(exclude = {"machineParameter"})
@Builder
public class MachineParameterValueEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String value;

    @ManyToOne// Eager fetching purposely
    @JoinColumns({
            @JoinColumn(name = "machine_id", referencedColumnName = "machine_id"),
            @JoinColumn(name = "parameter_id", referencedColumnName = "parameter_id")
    })
    private MachineParameterEntity machineParameter;

    @Column(name = "created_at")
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

}
