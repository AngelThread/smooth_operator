package com.smooth.operator.demo.entity;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public enum ParameterType {
    HEAT("heat", true, Double.class),
    MOISTURE("moisture", true, Double.class),
    POWER("power", false, Boolean.class),
    STIFFING("stuffing", false, String.class),
    QUANTITY("quantity", true, Integer.class),
    GLUE("glue", false, String.class);

    private String name;
    private boolean canBeMeasuredByUnit;
    private Class clazz;

    ParameterType(String name, boolean canBeMeasuredByUnit, Class clazz) {
        this.name = name;
        this.canBeMeasuredByUnit = canBeMeasuredByUnit;
        this.clazz = clazz;
    }

    public boolean checkParameterType(String value) {
        try {
            switch (this.getClazz().getSimpleName()) {
                case "Double":
                    Double.valueOf(value);
                    return true;
                case "Boolean":
                    return "true".equals(value.toLowerCase()) || "false".equals(value.toLowerCase());
                case "Integer":
                    Integer.valueOf(value);
                    return true;
                default:
                    return false;
            }
        } catch (Exception exception) {
            log.error("Wrong parameter type sent!", exception);
            return false;
        }
    }
}
