package com.smooth.operator.demo.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity()
@Table(name = "machine_parameters")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"parameter", "machine"})
@ToString(exclude = {"parameter", "machine"})
public class MachineParameterEntity implements Serializable {

    @EmbeddedId
    private MachineParameterId id;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("machine_id")
    @JoinColumn(name = "machine_id")
    private MachineEntity machine;


    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("parameter_id")
    private ParameterEntity parameter;

    @OneToMany(
            mappedBy = "machineParameter",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<MachineParameterValueEntity> values = new HashSet<>();

    @Column(name = "created_on")
    private LocalDateTime createdAt = LocalDateTime.now();

    public MachineParameterEntity(MachineEntity machine, ParameterEntity parameter) {
        this.machine = machine;
        this.parameter = parameter;
        this.id = new MachineParameterId(machine.getId(), parameter.getId());
    }

    public void addValue(MachineParameterValueEntity valueEntity) {
        this.values.add(valueEntity);
        valueEntity.setMachineParameter(this);
    }
}
