package com.smooth.operator.demo.entity;

import lombok.*;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "machines", indexes = {
        @Index(columnList = "key", name = "machine_key")})
//Our equals and hashcode method based on only the key which truly defines uniques in our case!
@EqualsAndHashCode(of = "key")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MachineEntity implements Serializable {
    /**
     * In the system we use auto-generated id instead of key to gain performance benefit of indexing.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * @NaturalId is the marking for Hibernate to understand this is the key known by outer world!
     * https://vladmihalcea.com/the-best-way-to-map-a-naturalid-business-key-with-jpa-and-hibernate/
     */
    @NaturalId
    @Column(nullable = false, unique = true)
    private String key;

    @Column(nullable = false)
    private String name;


    /**
     * Parameters belong to the machine.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "machine")
    @Builder.Default
    private Set<MachineParameterEntity> parameters = new HashSet<>();

    public void addParameter(ParameterEntity parameterEntity) {
        MachineParameterEntity machineParameterEntity = new MachineParameterEntity(this, parameterEntity);
        this.parameters.add(machineParameterEntity);
        parameterEntity.getMachines().add(machineParameterEntity);
    }

}
