package com.smooth.operator.demo.entity;

import lombok.*;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "parameters", indexes = {
        @Index(columnList = "key", name = "parameter_key")})
//Our equals and hashcode method based on only the key which truly defines uniques in our case!
@EqualsAndHashCode(of = "key")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParameterEntity implements Serializable {

    /**
     * In the system we use auto-generated id instead of key to gain performance benefit of indexing.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * @NaturalId is the marking for Hibernate to understand this is the key known by outer world!
     * https://vladmihalcea.com/the-best-way-to-map-a-naturalid-business-key-with-jpa-and-hibernate/
     */
    @NaturalId
    @Column(nullable = false, unique = true)
    private String key;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String type;

    private String unit;


    /**
     * MachineParameters belong to the parameter.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parameter")
    @Builder.Default
    private Set<MachineParameterEntity> machines = new HashSet<>();

}
