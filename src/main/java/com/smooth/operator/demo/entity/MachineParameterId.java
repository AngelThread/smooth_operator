package com.smooth.operator.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MachineParameterId implements Serializable {

    @Column(name = "machine_id")
    private Long machineId;

    @Column(name = "parameter_id")
    private Long parameterId;

}
