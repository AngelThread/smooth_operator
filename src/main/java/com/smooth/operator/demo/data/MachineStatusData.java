package com.smooth.operator.demo.data;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Map;

@Data
@NoArgsConstructor
public class MachineStatusData {
    //Represents the unique identifier of the machine
    @NonNull
    private String machineKey;
    @NonNull
    private Map<String, String> parameters;
}
