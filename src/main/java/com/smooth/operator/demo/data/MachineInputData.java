package com.smooth.operator.demo.data;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents a line of machines.csv
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({ "key", "name"}) //CSV header order.
public class MachineInputData {
    private String key;
    private String name;
}
