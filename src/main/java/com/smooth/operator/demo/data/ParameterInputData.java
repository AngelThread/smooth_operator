package com.smooth.operator.demo.data;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents a line of parameters.csv
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"key", "machineKey", "name", "type", "unit"}) //CSV header order.
public class ParameterInputData {
    private String key;
    private String machineKey;
    private String name;
    private String type;
    private String unit;
}
